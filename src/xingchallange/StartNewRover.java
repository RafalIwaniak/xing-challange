/**
 * On demand of XING interview challange.
 * @author Rafal Iwaniak
 */
package xingchallange;

import java.util.Scanner;

public class StartNewRover {

    int widthOfPlateu, lengthOfPlateu;
    int xCoordinate, yCoordinate;
    char heading;
    String order;
    String GeneralOutput = "";
    private static Rover rover;

    public void readOrders() {

        Scanner scanner = new Scanner(System.in);
        rover = Rover.returnSingleRover();
        try {

            widthOfPlateu = scanner.nextInt();
            lengthOfPlateu = scanner.nextInt();

            while (scanner.hasNextLine()) {
                xCoordinate = scanner.nextInt();
                yCoordinate = scanner.nextInt();
                heading = scanner.next().charAt(0);

                if (heading == 'N' | heading == 'E' | heading == 'S' | heading == 'W') {
                    scanner.nextLine();
                    order = scanner.nextLine();
                    rover.initRover(xCoordinate, yCoordinate, heading);
                    executeOrder(rover, order);
                } else {
                    GeneralOutput += "Unknown direction.\n";
                }
            }
        } catch (Exception e) {
            System.out.println("Finished. Thanks for our journey!");
        } finally {
            System.out.print(GeneralOutput);
            scanner.close();
        }
    }

    public void executeOrder(Rover rover, String order) {
        char singleOrder;
        boolean isDisaster = false;

        for (int i = 0; i < order.length(); i++) {
            singleOrder = order.charAt(i);

            if (singleOrder == 'L') rover.rotateLeft();
            if (singleOrder == 'R') rover.rotateRight();
            
            if (singleOrder == 'M') {
                rover.moveForward();
                isDisaster = checkPlateuBorder(rover, widthOfPlateu, lengthOfPlateu);
                if (isDisaster) {
                    GeneralOutput += "Crossed plateu border! Please send next rover. Disaster at: "
                            + rover.getXCoordinate() + " " + rover.getYCoordinate() + "\n";
                    break;
                }
            }
        }
        if (!isDisaster) {
            GeneralOutput += rover.getXCoordinate() + " " + rover.getYCoordinate()
                    + " " + rover.getHeading() + "\n";
        }
    }

    public boolean checkPlateuBorder(Rover rover, int widthOfPlateu, int lengthOfPlateu) {
        return rover.getXCoordinate() < 0
                || rover.getXCoordinate() > widthOfPlateu
                || rover.getYCoordinate() < 0
                || rover.getYCoordinate() > lengthOfPlateu;
    }
}
