/**
 * On demand of XING interview challange.
 * @author Rafal Iwaniak
 */

package xingchallange;

public class XINGChallange {

    public static void main(String[] args) {
        StartNewRover start = new StartNewRover();
        
        
        System.out.println("Write any word (different than orders) to finish.");
        start.readOrders();
    }
    
}
