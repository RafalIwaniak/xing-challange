/**
 * On demand of XING interview challange.
 * @author Rafal Iwaniak
 */

package xingchallange;

import java.util.Arrays;
import java.util.List;

public class Rover {
    private int xCoordinate;
    private int yCoordinate;
    private int index;
    private final List<Character> cardinal = Arrays.asList('N', 'E', 'S', 'W' );
    private char heading;
    
    private static Rover rover;
    public static Rover returnSingleRover() { 
        if (rover == null) 
            rover = new Rover();
        return rover; 
    }

    private Rover() { }
    public void initRover(int x, int y, char h) {
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.heading = h;
    }
    
    public int getXCoordinate() { return xCoordinate; }
    public int getYCoordinate() { return yCoordinate; }
    public char getHeading() { return heading; }
    
    public void setXCoordinate(int x) { this.xCoordinate = x; }
    public void setYCoordinate(int y) { this.yCoordinate = y; }
    public void setHeading(char h) { heading = h; }
    
    public void rotateRight() {
        searchIndex();
        index = (index + 1) % cardinal.size();
        this.heading = cardinal.get(index);
    }  
    
    public void rotateLeft() {
        searchIndex();
        index = (index - 1 + cardinal.size()) % cardinal.size();
        this.heading = cardinal.get(index);
    }

    public void searchIndex() {
        if (this.heading == 'N') this.index = 0;
        if (this.heading == 'E') this.index = 1;
        if (this.heading == 'S') this.index = 2;
        if (this.heading == 'W') this.index = 3;               
    }
    
    void moveForward() {
        if (getHeading() == 'N')
            setYCoordinate(getYCoordinate() + 1);
        if (getHeading() == 'E')
            setXCoordinate(getXCoordinate() + 1);
        if (getHeading() == 'S')
            setYCoordinate(getYCoordinate() - 1);
        if (getHeading() == 'W')
            setXCoordinate(getXCoordinate() - 1);     
    }
        
}
